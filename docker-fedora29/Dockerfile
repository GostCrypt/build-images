FROM fedora:29

MAINTAINER Nikos Mavrogiannopoulos <nmav@redhat.com>

ARG dnfflags="--allowerasing"

RUN dnf update -y
RUN dnf install $dnfflags -y make patch ccache git which autoconf libtool gettext-devel automake autogen nettle-devel libtpms-devel p11-kit-devel autogen-libopts-devel guile-devel guile22-devel libidn2-devel gawk gperf git2cl libtasn1-devel libtasn1-tools unbound-devel bison help2man xz net-tools rsync wget
RUN dnf install $dnfflags -y clang compiler-rt libseccomp-devel libasan libtsan libasan-static libubsan libubsan-static nodejs datefudge lcov openssl-devel dieharder mbedtls-utils openssl libcmocka-devel socat xz ppp abi-compliance-checker libabigail valgrind libunistring-devel clang-analyzer cppcheck lockfile-progs
RUN dnf install $dnfflags -y elfutils binutils wine.i686 mingw32-cmocka mingw32-p11-kit mingw32-nettle mingw32-libtasn1 mingw32-gcc mingw32-gmp mingw32-libidn2 util-linux expect softhsm
RUN dnf install $dnfflags -y dash wine mingw64-cmocka mingw64-nettle mingw64-libtasn1 mingw64-p11-kit mingw64-gcc mingw64-gmp mingw64-libidn2
RUN dnf install $dnfflags -y gtk-doc texinfo texinfo-tex texlive texlive-supertabular texlive-framed texlive-morefloats texlive-quotchap docbook5-style-xsl docbook-style-xsl ruby zip
#tlsfuzzer
RUN dnf install $dnfflags -y pycrypto python-six
# kcapi
RUN dnf install $dnfflags -y libkcapi-devel

# TMP testing; Install swtpm from testing repo as they are not yet in stable.
RUN dnf install $dnfflags -y nmap-ncat tpm-tools trousers-devel trousers swtpm

# Install nettle from koji as it is not yet in stable
RUN dnf install $dnfflags -y https://kojipkgs.fedoraproject.org//packages/mingw-nettle/3.4.1/1.fc29/noarch/mingw32-nettle-3.4.1-1.fc29.noarch.rpm https://kojipkgs.fedoraproject.org//packages/mingw-nettle/3.4.1/1.fc29/noarch/mingw64-nettle-3.4.1-1.fc29.noarch.rpm

RUN wget http://deb.debian.org/debian/pool/main/p/pmccabe/pmccabe_2.6.tar.gz
RUN tar xvf pmccabe_2.6.tar.gz && cd pmccabe && make && cp pmccabe /usr/local/bin

RUN mkdir -p /usr/local/
RUN git clone https://git.savannah.gnu.org/git/gnulib.git /usr/local/gnulib
ENV GNULIB_SRCDIR /usr/local/gnulib
ENV GNULIB_TOOL /usr/local/gnulib/gnulib-tool
